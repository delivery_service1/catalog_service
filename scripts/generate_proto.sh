#!/bin/bash

# Ensure the genproto directory exists
mkdir -p genproto

# Loop through all .proto files in the delivery_protos submodule folders
for file in delivery_protos/order_protos/*.proto delivery_protos/user_protos/*.proto delivery_protos/catalog_protos/*.proto; do
  if [ -f "$file" ]; then
    # Get the filename without the extension
    filename=$(basename -- "$file")
    filename_without_extension="${filename%.*}"

    # Run protoc for the current file
    protoc -I delivery_protos/ "$file" --go_out=paths=source_relative:genproto/ --go-grpc_out=paths=source_relative:genproto/
  fi
done
