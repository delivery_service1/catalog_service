package client

import (
	"catalog_service/config"
	"catalog_service/genproto/catalog_protos"
	"fmt"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	CategoryService() catalog_service.CategoryServiceClient
	ProductService() catalog_service.ProductServiceClient
}

type grpcClients struct {
	categoryService catalog_service.CategoryServiceClient
	productService catalog_service.ProductServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connCatalogService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting to api_gateway!",err.Error())
		return nil, err
	}

	return &grpcClients{
		categoryService: catalog_service.NewCategoryServiceClient(connCatalogService),
		productService: catalog_service.NewProductServiceClient(connCatalogService),
	}, nil
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient{
	return g.categoryService
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}