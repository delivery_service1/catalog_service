DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'status_enum') THEN
        CREATE TYPE status_enum AS ENUM ('yes', 'no');
    END IF;
END $$;


DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'product_type_enum') THEN
        CREATE TYPE product_type_enum AS ENUM ('modifier', 'product');
    END IF;
END $$;


 
CREATE TABLE IF NOT EXISTS categories (
    id uuid primary key,
    title varchar(40) unique,
    active status_enum,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS products (
    id uuid primary key,
    description text,
    order_number int,
    active status_enum,
    type product_type_enum,
    price numeric,
    category_id uuid references categories(id),
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at int DEFAULT 0
);

 