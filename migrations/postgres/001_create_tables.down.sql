DROP TYPE IF EXISTS status_enum;
DROP TYPE IF EXISTS product_type_enum;
 
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS products;
 