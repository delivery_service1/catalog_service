package service

import (
	pb "catalog_service/genproto/catalog_protos"
	"catalog_service/grpc/client"
	"catalog_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedCategoryServiceServer
}

func NewCategoryService(strg storage.IStorage, services client.IServiceManager) *categoryService {
	return &categoryService{
		storage: strg,
		serivces: services,
	}
}

func (c *categoryService) Create(ctx context.Context, request *pb.CreateCategoryRequest) (*pb.Category, error) {
	category, err := c.storage.Category().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}
	return category, nil
}

func (c *categoryService) Get(ctx context.Context, request *pb.CategoryPrimaryKey) (*pb.Category, error) {
	category, err := c.storage.Category().Get(ctx, request)
	if err != nil{
		fmt.Println("Err2",err.Error())
		return nil, err
	}
	return category, nil
}

func (c *categoryService) GetList(ctx context.Context, request *pb.GetCategoryListRequest) (*pb.CategoriesResponse, error) {
	categories, err := c.storage.Category().GetList(ctx, request)
	if err != nil{
		fmt.Println("Err 2", err.Error())
		return nil, err
	}
	return categories, nil
}

func (c *categoryService) Update(ctx context.Context, request *pb.UpdateCategory) (*pb.Category, error) {
	category, err := c.storage.Category().Update(ctx,request)
	if err != nil{
		fmt.Println("Err 3", err.Error())
		return nil, err
	}
	return category, nil
}

func (c *categoryService) Delete(ctx context.Context, key *pb.CategoryPrimaryKey) (*emptypb.Empty, error) {
	err := c.storage.Category().Delete(ctx,key)
	if err != nil{
		fmt.Println("Err 4", err.Error())
		return nil,err
	}
	return nil,nil
}


