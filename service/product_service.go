package service

import (
	pb "catalog_service/genproto/catalog_protos"
	"catalog_service/grpc/client"
	"catalog_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type productService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedCategoryServiceServer
	pb.UnimplementedProductServiceServer
}

func NewProductService(strg storage.IStorage, services client.IServiceManager) *productService {
	return &productService{
		storage: strg,
		serivces: services,
	}
}

func (c *productService) Create(ctx context.Context, request *pb.CreateProductRequest) (*pb.Product, error) {
	product, err := c.storage.Product().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating product!", err.Error())
		return nil, err
	}
	return product, nil
}

func (c *productService) Get(ctx context.Context, request *pb.ProductPrimaryKey) (*pb.Product, error) {
	product, err := c.storage.Product().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting product!",err.Error())
		return nil, err
	}
	return product, nil
}

func (c *productService) GetList(ctx context.Context, request *pb.GetProductListRequest) (*pb.ProductsResponse, error) {
	categories, err := c.storage.Product().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting list of products!", err.Error())
		return nil, err
	}
	return categories, nil
}

func (c *productService) Update(ctx context.Context, request *pb.UpdateProduct) (*pb.Product, error) {
	product, err := c.storage.Product().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating product!", err.Error())
		return nil, err
	}
	return product, nil
}

func (c *productService) Delete(ctx context.Context, key *pb.ProductPrimaryKey) (*emptypb.Empty, error) {
	err := c.storage.Product().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting product!", err.Error())
		return nil,err
	}
	return nil,nil
}


