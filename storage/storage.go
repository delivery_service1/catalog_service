package storage

import (
	pb "catalog_service/genproto/catalog_protos"
	"context"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pb.CreateCategoryRequest) (*pb.Category, error)
	Get(context.Context, *pb.CategoryPrimaryKey) (*pb.Category, error)
	GetList(context.Context, *pb.GetCategoryListRequest) (*pb.CategoriesResponse, error)
	Update(context.Context, *pb.UpdateCategory) (*pb.Category, error)
	Delete(context.Context, *pb.CategoryPrimaryKey) (error)
}

type IProductStorage interface {
	Create(context.Context, *pb.CreateProductRequest) (*pb.Product, error)
	Get(context.Context, *pb.ProductPrimaryKey) (*pb.Product, error)
	GetList(context.Context, *pb.GetProductListRequest) (*pb.ProductsResponse, error)
	Update(context.Context, *pb.UpdateProduct) (*pb.Product, error)
	Delete(context.Context, *pb.ProductPrimaryKey) (error)
}

