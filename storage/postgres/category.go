package postgres

import (
	pb "catalog_service/genproto/catalog_protos"
	"catalog_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type categoryRepo struct {
	DB *pgxpool.Pool
}

func NewCategoryRepo (db *pgxpool.Pool) storage.ICategoryStorage{
	return &categoryRepo{
		DB: db,
	}
}

func (c *categoryRepo) Create(ctx context.Context, createCategory *pb.CreateCategoryRequest) (*pb.Category, error) {
	var (
		id = uuid.New()
		category = pb.Category{}
	)

	query := `
		insert into categories (id, title, active)
			values ($1, $2, $3)
			returning id, title, active, created_at::text `

	err := c.DB.QueryRow(ctx, query,
		 	id, 
			createCategory.GetTitle(), 
			createCategory.GetActive(),
		).Scan(
			&category.Id,
			&category.Title,
			&category.Active,
			&category.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating category!", err.Error())
			return nil, err
	 	} 

		return &category, nil
}

func (c *categoryRepo) Get(ctx context.Context, key *pb.CategoryPrimaryKey) (*pb.Category, error) {
	var (
		category = pb.Category{}
	)

	query := `select id, title, active, created_at::text, updated_at::text from categories
		where deleted_at = 0 and id = $1 `

	err := c.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&category.Id,
		&category.Title,
		&category.Active,
		&category.CreatedAt,
		&category.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting category by id!", err.Error())
		return nil, err
	}

	return &category, nil
}

// SHOULD CORRECT THE SEARCH OF active.

func (c *categoryRepo) GetList(ctx context.Context, request *pb.GetCategoryListRequest) (*pb.CategoriesResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		categories = pb.CategoriesResponse{}
		filter, countQuery string
		search = request.Search
	)
	// search: title, active

	countQuery = `select count(1) from categories WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND title ilike '%%%s%%' OR active::text ilike '%%%s%%' `,search, search)
	
	if search != ""{
		countQuery += filter
	}

	err := c.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of categories",err.Error())
		return nil, err
	}

	query :=  `select id, title, active, created_at::text, updated_at::text from categories
	where deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := c.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting categories!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			category = pb.Category{}
		)

		err = rows.Scan(
			 &category.Id,
			 &category.Title,
			 &category.Active,
			 &category.CreatedAt,
			 &category.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning categories!",err.Error())
			return nil, err
		}
		categories.Categories = append(categories.Categories, &category)
	}

	categories.Count = count

	return &categories, nil
}

func (c *categoryRepo) Update(ctx context.Context, updCategory *pb.UpdateCategory) (*pb.Category, error) {
	category := pb.Category{}

	query := `UPDATE categories set title = $1, active = $2 
			where id = $3 
		returning id, title, active `
	err := c.DB.QueryRow(ctx, query,
		updCategory.GetTitle(), 
		updCategory.GetActive(),
		updCategory.GetId(),
	).Scan(
		&category.Id,
		&category.Title,
		&category.Active,
	)

	if err != nil{
		fmt.Println("error while updating categories!", err.Error())
		return nil, err
	}

	return &category, nil
}

func (c categoryRepo) Delete(ctx context.Context, key *pb.CategoryPrimaryKey) (error) {
	query := `UPDATE categories set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := c.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting category by id!", err.Error())
		return err
	}
	return nil
}