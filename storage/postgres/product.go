package postgres

import (
	pb "catalog_service/genproto/catalog_protos"
	"catalog_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productRepo struct {
	DB *pgxpool.Pool
}

func NewProductRepo (db *pgxpool.Pool) storage.IProductStorage{
	return &productRepo{
		DB: db,
	}
}

func (p *productRepo) Create(ctx context.Context, createProduct *pb.CreateProductRequest) (*pb.Product, error) {
	var (
		id = uuid.New()
		product = pb.Product{}
	)

	query := `
		insert into products (id, description, order_number, active,
		type, price, category_id)
			values ($1, $2, $3, $4, $5, $6, $7)
				returning id, description, order_number, active, type, price,
					category_id, created_at::text `

	err := p.DB.QueryRow(ctx, query,
		 	id, 
			createProduct.GetDescription(), 
			createProduct.GetOrderNumber(),
			createProduct.GetActive(),
			createProduct.GetType(),
			createProduct.GetPrice(),
			createProduct.GetCategoryId(),
		).Scan(
			&product.Id,
			&product.Description,
			&product.OrderNumber,
			&product.Active,
			&product.Type,
			&product.Price,
			&product.CategoryId,
			&product.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating product!", err.Error())
			return nil, err
	 	} 

		return &product, nil
}

func (p *productRepo) Get(ctx context.Context, key *pb.ProductPrimaryKey) (*pb.Product, error) {
	var (
		product = pb.Product{}
	)

	query := `select id, description, order_number, active, type,
		price, category_id,
			created_at::text, updated_at::text from products
		where deleted_at = 0 and id = $1 `

	err := p.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&product.Id,
		&product.Description,
		&product.OrderNumber,
		&product.Active,
		&product.Type,
		&product.Price,
		&product.CategoryId,
		&product.CreatedAt,
		&product.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting product by id!", err.Error())
		return nil, err
	}

	return &product, nil
}

// SHOULD CORRECT THE SEARCH OF active.

func (p *productRepo) GetList(ctx context.Context, request *pb.GetProductListRequest) (*pb.ProductsResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		products = pb.ProductsResponse{}
		filter, countQuery string
		search = request.Search
	)
	// search: description, type, active, price, order_number

	countQuery = `select count(1) from products WHERE deleted_at = 0 ` 

	
	filter = fmt.Sprintf(` AND description ilike '%%%s%%' OR order_number::text ilike '%%%s%%' OR 
	active::text ilike '%%%s%%' OR type::text ilike '%%%s%%' OR price::text ilike '%%%s%%' `,
	search, search, search, search, search)


	if search != ""{ 
		countQuery += filter
	}

	err := p.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of users",err.Error())
		return nil, err
	}

	query :=   `select id, description, order_number, active, type,
	price, category_id,
		created_at::text, updated_at::text from products
	where deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := p.DB.Query(ctx, query,int64(offset), request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting products!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			product = pb.Product{}
		)

		err = rows.Scan(
			&product.Id,
			&product.Description,
			&product.OrderNumber,
			&product.Active,
			&product.Type,
			&product.Price,
			&product.CategoryId,
			&product.CreatedAt,
			&product.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning products!",err.Error())
			return nil, err
		}
		products.Products = append(products.Products, &product)
	}

	products.Count = count

	return &products, nil
}

func (p *productRepo) Update(ctx context.Context, updProduct *pb.UpdateProduct) (*pb.Product, error) {
	product := pb.Product{}

	query :=  `UPDATE products SET description = $1, order_number = $2, active = $3, type = $4, price = $5,
					category_id = $6, updated_at = now() where id = $7 AND deleted_at = 0 
						returning id, description, order_number, active, type, price, category_id, 
							updated_at::text `
	err := p.DB.QueryRow(ctx, query,
		updProduct.GetDescription(),
		updProduct.GetOrderNumber(),
		updProduct.GetActive(),
		updProduct.GetType(),
		updProduct.GetPrice(),
		updProduct.GetCategoryId(),
		updProduct.GetId(),
	).Scan(
		&product.Id,
		&product.Description,
		&product.OrderNumber,
		&product.Active,
		&product.Type,
		&product.Price,
		&product.CategoryId,
		&product.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating products!", err.Error())
		return nil, err
	}

	return &product, nil
}

func (p productRepo) Delete(ctx context.Context, key *pb.ProductPrimaryKey) (error) {
	query := `UPDATE products set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := p.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting product by id!", err.Error())
		return err
	}
	return nil
}